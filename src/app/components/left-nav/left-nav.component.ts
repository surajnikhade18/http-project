import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss'],
})
export class LeftNavComponent implements OnInit {
  // this any is for convenience, PLZ dont use in real life project
  users: any = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    // console.log('before API');
    // // NEVER DO THIS.
    // this.http.get('https://jsonplaceholder.typicode.com/users').subscribe((data: any) => {
    //   this.users = data;
    // });
    // console.log('after API');
  }
}
