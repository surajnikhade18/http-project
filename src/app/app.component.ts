import { Component, OnInit } from '@angular/core';
import { AppConfigService } from './app-config-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  apiBaseUrl!: string;

  constructor(private appConfigService: AppConfigService) {}
  ngOnInit() {
    this.apiBaseUrl = this.appConfigService.apiBaseUrl;
    console.log(this.apiBaseUrl);
  }
}
